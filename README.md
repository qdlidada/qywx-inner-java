## 企业微信自自建内部应用
后端项目部署手册
```shell
# 1.克隆后端项目
git clone git@gitee.com:gblfy/qywx-inner-java.git

# 2.配置数据库信息
找到项目resources下面application-dev.yml文件中的数据库配置，保持默认，用户名密码，根据实际情况进行更新即可

# 3.初始化数据库脚本
找到项目doc下面的qywx_inner_java.sql，进行脚本初始化

# 4.初始化数据库脚本
找到本地hosts文件，配置本地域名映射（因为腾讯企业微信需要回调前端项目认证地址，因此，需要设置域名）
127.0.0.1 apitest.tobdev.com
127.0.0.1 test.tobdev.com

# 5.运行项目
项目入口：com.gblfy.qywxinner.QywxInnerApplication
```

企业微信文档：
https://developer.work.weixin.qq.com/document/path/91039
![img.png](pic/img.png)

企业微信管控台：https://work.weixin.qq.com/wework_admin/loginpage_wx
![img_1.png](pic/img_1.png)