/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : localhost:3306
 Source Schema         : qywx_inner_java

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 26/02/2022 21:12:46
*/

create database qywx_inner_java;
use qywx_inner_java;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for qywx_inner_company
-- ----------------------------
DROP TABLE IF EXISTS `qywx_inner_company`;
CREATE TABLE `qywx_inner_company`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `app_id` int UNSIGNED NOT NULL DEFAULT 1 COMMENT 'tobdev应用id 可以不写',
  `corp_id` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '企业id',
  `agent_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '授权应用id',
  `agent_secret` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '应用密钥',
  `agent_token` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'token',
  `agent_encoding_aes_key` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'encoding_aes_key',
  `corp_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '企业名称',
  `corp_full_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '企业全称',
  `subject_type` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '企业类型',
  `verified_end_time` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '企业认证到期时间',
  `approval_template_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '审批流程引擎模板id',
  `status` tinyint NULL DEFAULT 0 COMMENT '账户状态，-1为删除，禁用为0 启用为1',
  `addtime` int UNSIGNED NULL DEFAULT 0 COMMENT '创建时间',
  `modtime` int UNSIGNED NULL DEFAULT 0 COMMENT '修改时间',
  `rectime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '变动时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '企业微信自建内部应用公司' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qywx_inner_company
-- ----------------------------
INSERT INTO `qywx_inner_company` VALUES (1, 1, 'wwea98220fdcd8a38d', '1000002', '4o3gWg_fOzFP0pL0_sJ8zmwwrRMKzBAE-SZhz8JJD24', '', '', '泽昕科技', '泽昕科技股份有限公司', '', '', '', 1, 0, 0, '2022-02-26 21:11:24');

-- ----------------------------
-- Table structure for qywx_third_user
-- ----------------------------
DROP TABLE IF EXISTS `qywx_third_user`;
CREATE TABLE `qywx_third_user`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `corp_id` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '企业id',
  `user_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户id',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '部门名称',
  `parentid` int NOT NULL DEFAULT 0 COMMENT '父部门id',
  `position` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '职位',
  `gender` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '性别',
  `email` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '邮箱',
  `is_leader_in_dept` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '是否是部门负责人',
  `avatar` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '头像',
  `thumb_avatar` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '头像缩略图',
  `telephone` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '电话',
  `alias` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '别吃饭去',
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '地址',
  `open_userid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'open_userid',
  `main_department` int NOT NULL DEFAULT 0 COMMENT '主部门id',
  `qr_code` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '二维码',
  `status` tinyint NULL DEFAULT 0 COMMENT '状态，-1为删除，禁用为0 启用为1',
  `addtime` int UNSIGNED NULL DEFAULT 0 COMMENT '创建时间',
  `modtime` int UNSIGNED NULL DEFAULT 0 COMMENT '修改时间',
  `rectime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '变动时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '企业微信人员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qywx_third_user
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
