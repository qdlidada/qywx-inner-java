package com.gblfy.qywxinner.service;

import com.gblfy.qywxinner.mapper.QywxInnerCompanyMapper;
import com.gblfy.qywxinner.model.entity.QywxInnerCompany;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class QywxInnerCompanyService {

    private final static Logger logger = LoggerFactory.getLogger("test");

    @Autowired
    private QywxInnerCompanyMapper qywxInnerCompanyMapper;

    @Autowired
    private QywxInnerDepartmentService qywxInnerDepartmentService;
    @Autowired
    private QywxInnerUserService qywxInnerUserService;

    public QywxInnerCompany getCompanyByCorpId(String corpId) {
        return qywxInnerCompanyMapper.getCompanyByCorpId(corpId);
    }

    public Integer saveCompany(QywxInnerCompany company) {
        return qywxInnerCompanyMapper.saveCompany(company);
    }


    public Boolean deleteCompanyByCorpId(String corpId) {
        //删除公司
        Integer comResult = qywxInnerCompanyMapper.deleteCompanyByCorpId(corpId);

//        //删除部门
//        Boolean deptResult =   qywxInnerDepartmentService.deleteDepartmentByCorpId(corpId);
        //删除人
        Boolean usrResult = qywxInnerUserService.deleteUserByCorpId(corpId);

        if (comResult >= 0) {
            return true;
        }

        return false;
    }


}
