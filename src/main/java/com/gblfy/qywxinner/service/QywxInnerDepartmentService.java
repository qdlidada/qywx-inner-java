package com.gblfy.qywxinner.service;

import com.gblfy.qywxinner.mapper.QywxInnerDepartmentMapper;
import com.gblfy.qywxinner.model.entity.QywxInnerDepartment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class QywxInnerDepartmentService  {

    // private final static Logger logger = LoggerFactory.getLogger("test");

    @Autowired
    private QywxInnerDepartmentMapper qywxInnerDepartmentMapper;


    public QywxInnerDepartment getDepartmentByCorpId(String corpId) {
        return qywxInnerDepartmentMapper.getDepartmentByCorpId(corpId);
    }

    public Integer saveDepartment(QywxInnerDepartment Department){
        return qywxInnerDepartmentMapper.saveDepartment(Department);
    }

    public Boolean deleteDepartmentByCorpId(String corpId){
        Integer result = qywxInnerDepartmentMapper.deleteDepartmentByCorpId(corpId);
        return  false;
    }


}
