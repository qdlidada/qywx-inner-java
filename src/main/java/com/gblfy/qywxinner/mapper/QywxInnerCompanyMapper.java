package com.gblfy.qywxinner.mapper;

import com.gblfy.qywxinner.model.entity.QywxInnerCompany;
import org.apache.ibatis.annotations.Param;


public interface QywxInnerCompanyMapper {

    QywxInnerCompany  getCompanyByCorpId(@Param("corp_id") String corpId);

    Integer  saveCompany(QywxInnerCompany company);

    Integer deleteCompanyByCorpId(@Param("corp_id") String corpId);

}
